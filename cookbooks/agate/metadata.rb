name             'agate'
maintainer       'oDesk'
maintainer_email 'sanmah@odesk.com'
license          'All rights reserved'
description      'Installs/Configures ui applet project'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apt-repo"

recipe "ui-applet::vinstall", "Install required packages for ui-applet (vagrant)."
recipe "ui-applet::vdeploy", "Takes required steps for ui-applet deployment (vagrant)."

