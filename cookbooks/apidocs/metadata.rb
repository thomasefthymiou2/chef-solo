name             'apidocs'
maintainer       'oDesk'
maintainer_email 'ptsakiri@odesk.com'
license          'All rights reserved'
description      'Installs/Configures taskmanager project'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

recipe "apidocs::deploy", "Deploys apidocs"