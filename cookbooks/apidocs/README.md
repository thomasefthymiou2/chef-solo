Api Documentation Cookbook
==============

Bundle cookbooks for s3:
-------------
 
	cd cookbooks/ && zip -r ../cookbooks.zip * && cd ..
	aws s3 cp cookbooks.zip s3://odesk-chef-solo-bucket/cookbooks.zip
