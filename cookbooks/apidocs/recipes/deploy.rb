#
# Cookbook Name::apidocs
# Recipe::deploy
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

node[:deploy].each do |app_name, deploy|

	bash "bootstrap" do
		cwd "#{deploy[:deploy_to]}/current/"
		code <<-EOH
		make bootstrap
		EOH
	end

	bash "build" do
		cwd "#{deploy[:deploy_to]}/current/"
		code <<-EOH
		make build API_BASE="https://www.odesk.com"
		EOH
	end
  
end
