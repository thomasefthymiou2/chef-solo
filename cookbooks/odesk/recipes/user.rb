#group "odusers" do
#  gid 997
#end


group "ops" do
  gid 998
end

#group "admins" do
#  gid     999
#end


USERS = []
ADMINS = []

def odesk_user_def(*params)
  allusers = params[0]
  node_roles = [params[1]].flatten

  allusers.each do |user|
    # Set `login` to the id of the data bag item
    login = user["id"]

    home = "/home/#{login}"

    # check user status and intersection of user roles and node roles, and
    #   remove users from system that are either disabled or should not have
    #   access
    user['access_roles'] ||= []
    user['disabled'] ||= false
    user['is_admin'] ||= false

    Chef::Log.info("evaluating #{user} - admin: #{user['is_admin']}, disabled?: #{user['disabled']}, access_roles: #{user['access_roles'].join(',')}")

    # if (user['disabled'] ||
    #     ((user['access_roles'] & node_roles).empty? && !user['is_admin'])
    # )
    #   user(login) do
    #     action    :remove
    #   end
    #   directory "/home/#{login}" do
    #     action    :delete
    #     recursive true
    #   end

    # else

    # build up list of currently active users
    USERS << login
    ADMINS << login if ((user['is_admin']) && (user['is_admin'] == true))

    # for each admin in the data bag, make a user resource
    # to ensure they exist
    user(login) do
      uid user['uid']
      gid user['gid']
      shell user['shell']
      comment user['comment']
      password user['password'] if user.has_key? 'password'

      home home
      supports :manage_home => true
      action [:create, :modify]
    end

    directory home do
      owner user['uid']
      group user['gid']
      mode "0700"
      action :create
    end

    directory "#{home}/.ssh" do
      owner user['uid']
      group user['gid']
      mode "0700"
      action :create
    end

    template "#{home}/.ssh/authorized_keys" do
      source "authorized_keys.erb"
      mode 0400
      owner user['uid']
      group user['uid']
      variables(
          :public_keys => user['public_keys']
      )
    end
    #end

  end

end


a = apt_package "build-essential" do
  action :nothing
end
a.run_action(:install)

b = apt_package "libxslt-dev" do
  action :nothing
end
b.run_action(:install)

c = apt_package "libxml2-dev" do
  action :nothing
end
c.run_action(:install)

d = apt_package "libxml2" do
  action :nothing
end
d.run_action(:install)


chef_gem "aws-sdk" do
  action :install
end

require 'aws-sdk'


s3_users = []
AWS.config(:access_key_id => "AKIAILJTOH4JHQSPM5VQ",
           :secret_access_key => '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',
           :s3_endpoint => 's3-us-west-1.amazonaws.com')
s3 = AWS::S3.new
s3.buckets['odesk-chef-solo-bucket'].objects.with_prefix('users/').each do |object|
  begin
    s3_users << JSON(object.read)
  rescue
    puts object.key + "error"
  end
end

odesk_user_def s3_users, [] #node[:authorization]["access_roles"]

# An empty array to which we'll add the admins' logins as we go.

# Create an "admins" group on the system
# You might use this group in the /etc/sudoers file
# to provide sudo access to the admins

#group "odusers" do
#  gid 997
#  members (ADMINS | USERS) if (ADMINS.size > 0 || USERS.size > 0)
#end

#group "admins" do
#  gid 999
#  members ADMINS if ADMINS.size > 0
#end
