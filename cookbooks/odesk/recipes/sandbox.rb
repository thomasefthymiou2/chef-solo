# core software

#include_recipe "openldap::auth"

nginx_site 'default' do
  enable false
end

apache_site 'default' do
  enable false
end
  
if platform_family?('debian')
  pkgs = %W[postgresql-server-dev-9.1 perlmagick cpanminus pmtools perl-modules perl-doc ruby-compass
    subversion git git-core debconf-utils memcached php5 php-date php5-curl
    php5-memcached php5-memcache php-crypt-blowfish joe lynx-cur ack-grep screen]

  # mandatory perl modules 
  pkgs.concat %W[libapache-dbi-perl libarchive-zip-perl libcache-memcached-fast-perl
    libclass-accessor-perl libcrypt-blowfish-perl libcrypt-openssl-rsa-perl
    libdate-calc-perl libdate-manip-perl libdatetime-perl libdbd-pg-perl
    libdigest-hmac-perl liberror-perl libgd-gd2-perl libgd-text-perl
    libhtml-format-perl libhtml-template-expr-perl libio-string-perl
    libio-stringy-perl liblwp-protocol-https-perl libmail-box-perl
    libmime-tools-perl libmime-types-perl libparse-yapp-perl libsoap-lite-perl
    libspreadsheet-writeexcel-perl libtemplate-perl libtimedate-perl
    libunix-syslog-perl libxml-parser-perl libxml-perl libxml-regexp-perl
    libxml-simple-perl libyaml-libyaml-perl libcrypt-eksblowfish-perl]
  
  # additional dependencies for non-packaged modules 
  pkgs.concat %W[libclass-loader-perl libconvert-ascii-armour-perl libcrypt-cbc-perl
    libcrypt-des-perl libdata-buffer-perl libdigest-md2-perl libsort-versions-perl
    libtie-encryptedhash-perl libyaml-perl libglib2.0-dev libgtop2-dev
    build-essential python-software-properties python g++ make
    libapache2-svn php-apc]
    
  pkgs.each do |pkg|
    package pkg do
      action :install
    end
  end

  include_recipe 'apt-repo'
  ppa 'chris-lea/node.js-legacy'

  execute "apt-get update"
  %w[nodejs npm].each do |pkg|
    package pkg do
      action :install
    end
  end
   
  execute "npm install -g grunt-cli" do
    not_if "grunt --version | grep 0.1.9"
  end

  execute "cpanm --skip-installed -f Digest::SHA256"
  execute "install perl modules using cpanm" do
    command "env MATHPARI_USEFTP=TRUE cpanm --skip-installed App::pmuninstall Linux::Pid Env::C GTop"
  end    

  execute "cpanm --skip-installed /vagrant/lib/OdeskEnv"

  execute "add 'apache' user" do
    command "useradd -o -u $(id -u www-data) apache -M -N"
    not_if "id apache"
  end

  execute "add 'apache' group" do
    command "groupadd -o -g $(id -g www-data) apache"
    not_if "grep apache /etc/group"
  end
  
  execute "mkdir -p /usr/local/odesk/etc" do
    not_if { ::File.directory?('/usr/local/odesk/etc') }
  end

  execute "create /usr/local/odesk/etc/users" do
    command "bash -c 'echo vagrant:1  > /usr/local/odesk/etc/users'"
    not_if { ::File.exists?('/usr/local/odesk/etc/users') }
  end

  execute "add precise32.odesk.com to /etc/hosts" do
    command "perl -pi -e's/precise32/precise32 precise32.odesk.com/' /etc/hosts"
    not_if "grep precise32.odesk.com /etc/hosts"
  end

  execute "copy Apache/SizeLimit/Core.pm" do
    command %[dpkg-query -L libapache2-mod-perl2 | grep SizeLimit.pm | perl -MFile::Path=make_path -MFile::Copy -ne'chomp;s{^(.+)2(/SizeLimit)\.pm}{$1$2};make_path($_);copy("/vagrant/lib/Apache/SizeLimit/Core.pm",$_)']
    not_if %[perl -MApache::SizeLimit::Core -le'print "ok"']
  end

  execute "set DNS" do
    command "perl -pi -e's/(nameserver).+$/$1 172.27.1.92/' /etc/resolv.conf"
    not_if "grep 172.27.1.92 /etc/resolv.conf"
  end

  execute %[add "search odesk.com"] do
    command %[echo "search odesk.com" >> /etc/resolv.conf]
    not_if %[grep search /etc/resolv.conf | grep odesk.com]
  end 
end
