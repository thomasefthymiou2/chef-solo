#
# Cookbook Name:: taskmanager
# Recipe:: deploy
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

node[:deploy].each do |app_name, deploy|
  
  bash "install_composer" do
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    curl -s https://getcomposer.org/installer | php
    php composer.phar install --no-dev
    EOH
  end

  directory "#{deploy[:deploy_to]}/current/data/logs" do
    owner deploy[:user]
    group deploy[:group]
    mode "0777"
  end

  directory "#{deploy[:deploy_to]}/current/public/static" do
    owner deploy[:user]
    group deploy[:group]
    mode "0777"
  end

  template "#{deploy[:deploy_to]}/current/config/autoload/local.php" do
    source "local.php.erb"
    mode 0644
    owner deploy[:user]
    group deploy[:group]
  end
  
  bash "compass_compile" do
    user "root"
    cwd "#{deploy[:deploy_to]}/current/vendor/odesk/ui"
    code <<-EOH
    compass compile css/v3/default    
    EOH
  end

  bash "compile_assets" do
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH    
    php ./public/index.php compile-assets
    EOH
  end
  
end