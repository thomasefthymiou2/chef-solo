#
# Cookbook Name::taskmanager
# Recipe::vinstall
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

current_path = "/home/vagrant/#{node['project_path']}"

web_app "#{node['project']}_app" do
  server_name "#{node['project']}_app"
  docroot "#{current_path}/"  
  allow_override "All"
end

bash "Setup default PHP timezone" do
  user "root"
  code <<-EOH
  sed -i 's/;date.timezone.*/date.timezone = UTC/g' `php -r 'echo php_ini_loaded_file();'`
  EOH
end

bash "install_compass" do
  cwd "/tmp"
  code <<-EOH  
  gem update --system
  gem install sass --version=3.2.12
  gem install compass
  EOH
end

# install uglifycss and uglifyjs with npm
bash "install_uglify_components" do  
  cwd "/tmp"
  code <<-EOH  
  npm install uglify-js -g
  npm install uglifycss -g
  EOH
end