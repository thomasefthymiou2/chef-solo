#
# Cookbook Name::taskmanager
# Recipe:: default
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

# install 3.2.12 sass, by default 3.3.0.rc.2 is installed which fails
bash "install_compass" do
  user "root"
  cwd "/tmp"
  code <<-EOH  
  gem update --system
  gem install sass --version=3.2.12
  gem install compass
  EOH
end

# add apt-source for latest nodejs
include_recipe 'apt-repo'
ppa 'chris-lea/node.js'

execute "apt-get update"

#install node and npm
package "nodejs" do
  action :install
end

# install uglifycss and uglifyjs with npm
bash "install_uglify_components" do  
  cwd "/tmp"
  code <<-EOH  
  npm install uglify-js -g
  npm install uglifycss -g
  EOH
end

include_recipe 'newrelic'