#
# Cookbook Name:: taskmanager
# Recipe:: sf2deploy
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

node[:deploy].each do |app_name, deploy|

  include_recipe 'newrelic::php-agent'

  template "#{deploy[:deploy_to]}/current/app/config/parameters.yml" do
    source "parameters.yml.erb"
    mode 0644
    owner deploy[:user]
    group deploy[:group]
  end

  bash "Setup default PHP timezone" do
    user "root"
    code <<-EOH
    sed -i 's/;date.timezone.*/date.timezone = UTC/g' `php -r 'echo php_ini_loaded_file();'`
    EOH
  end

  bash "install_composer" do
    user deploy[:user]
    group deploy[:group]
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    curl -s https://getcomposer.org/installer | php
    php composer.phar install --no-dev --no-interaction
    EOH
  end  

  bash "compass_compile" do
    user deploy[:user]
    group deploy[:group]
    cwd "#{deploy[:deploy_to]}/current/vendor/odesk/ui"
    code <<-EOH
    compass compile css/v3/default    
    EOH
  end

  %w[ app/logs app/cache web/assets web/bundles ].each do |path|
    directory "#{deploy[:deploy_to]}/current/#{path}" do
      owner node[:apache][:user]
      group deploy[:group]
      mode 0744
      recursive true
    end
  end  

  bash "compile_assets" do
    user node[:apache][:user]
    group deploy[:group]
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH    
    php app/console cache:clear --env=prod
    php app/console assetic:dump --env=prod --no-debug
    EOH
  end
  
end