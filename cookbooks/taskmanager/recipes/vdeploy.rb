#
# Cookbook Name::taskmanager
# Recipe::vdeploy
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

current_path = "/home/vagrant/#{node['project_path']}"

template "#{current_path}/app/config/parameters.yml" do
  source "vparameters.yml.erb"
  mode 0644
  variables({
  :statsdEnabled => true,
  :statsdEnv => 'dev',
  :statsdHost => 'statsd.odesk.com',
  :statsdPort => 8125,    
  :cookiePrefix => 'dev02_',
  :webRoot => 'http://dev02.odesk.com:23850/',
  :helpCmsRoot => 'http://dev02.odesk.com:23850/helpcms/',
  :cookieDomain => '.dev02.odesk.com',
  :uglifyjs2_bin =>'/usr/local/bin/uglifycss',
  :uglifycss_bin => '/usr/local/bin/uglifyjs',
  :agoraCalTimeout => 50,
  :taskgenieURL => 'http://front-dev.agora.odesk.com:8090/',
  :orpcURL => 'http://dev02.odesk.com:23850/orpc/',
  :navURL => 'http://dev02.odesk.com:23850'
})

end

bash "install_composer" do
  cwd "#{current_path}"
  code <<-EOH
  curl -s https://getcomposer.org/installer | php
  php composer.phar install
  EOH
end  

bash "compass_compile" do
  cwd "#{current_path}/vendor/odesk/ui"
  code <<-EOH
  compass compile css/v3/default    
  EOH
end

%w[ app/logs app/cache web/assets web/bundles ].each do |path|
  directory "#{current_path}/current/#{path}" do
    mode 0744    
    recursive true
  end
end