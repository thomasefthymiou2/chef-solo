TaskManager Cookbook
==============

Bundle cookbooks for s3:
-------------
 
	cd cookbooks/ && zip -r ../cookbooks.zip * && cd ..
	aws s3 cp cookbooks.zip s3://odesk-chef-solo-bucket/cookbooks.zip

Creating a TM UI Stack in Amazon Opsworks:
-------------

	aws opsworks create-stack \
		--region us-east-1 \
		--name "taskmanager-ui-$ENV" \
		--stack-region us-west-1 \
		--vpc-id vpc-86c60eef \
		--service-role-arn "arn:aws:iam::739939173819:role/aws-opsworks-service-role" \
		--default-instance-profile-arn "arn:aws:iam::739939173819:instance-profile/aws-opsworks-ec2-role" \
		--default-os "Ubuntu 12.04 LTS" \
		--default-subnet-id $SUBNET \
		--custom-json '{"domain": "odesk.com","openldap": {"basedn": "dc=odesk,dc=com","server": "ldap.odesk.com"},"statsd": {"enabled": true, "environment": "prod", "host": "statsd.odesk.com", "port": 8125 }, "newrelic": { "license": "b93eb98c61d9c195c85f188786271c29300780d1", "application_monitoring": { "enabled": true, "appname": "taskmanager-ui.prod"}, "php-agent": { "php_recipe": "php::configure", "config_file" : "/etc/php5/apache2/conf.d/newrelic.ini" } },"is_dev_env": "false","static_url":"/taskmanager/static/", "static_path":"web/assets/","uglifyjs2_bin": "/usr/bin/uglifyjs","uglifycss_bin": "/usr/bin/uglifycss", "root_ext": "web", "taskmanager": { "cookiePrefix": "", "webRoot": "https://www.odesk.com/", "helpCmsRoot": "https://www.odesk.com/helpcms/", "cookieDomain": ".odesk.com", "agoraCalTimeout": 5, "taskgenieURL": "http://front.agora.odesk.com:8090/", "orpcURL": "http://orpc.prod.sv4.odesk.com:83/orpc/", "navURL": "https://www.odesk.com"}}' \
		--use-custom-cookbooks \
		--custom-cookbooks-source Url="https://odesk-chef-solo-bucket.s3.amazonaws.com/cookbooks.zip",Username="AKIAILJTOH4JHQSPM5VQ",Password="ywCa8bBVdO+7ce+VZSjQX108+o5r9GUoOWbGuMxl",Type=s3 \
		--configuration-manager Name=Chef,Version=11.4 \
		--default-ssh-key-name ptsakiri

Note: depending on the environment the stack's custom json is different. Please use the references below for the according environment: 

*Production*

	--custom-json '{"domain": "odesk.com","openldap": {"basedn": "dc=odesk,dc=com","server": "ldap.odesk.com"}, "statsd": {"enabled": true, "environment": "prod", "host": "statsd.odesk.com", "port": 8125 }, "newrelic": { "license": "b93eb98c61d9c195c85f188786271c29300780d1", "application_monitoring": { "enabled": true, "appname": "taskmanager-ui.prod"}, "php-agent": { "php_recipe": "php::configure", "config_file" : "/etc/php5/apache2/conf.d/newrelic.ini" } }, "is_dev_env": "false","static_url":"/taskmanager/static/", "static_path":"web/assets/","uglifyjs2_bin": "/usr/bin/uglifyjs","uglifycss_bin": "/usr/bin/uglifycss", "root_ext": "web", "taskmanager": { "cookiePrefix": "", "webRoot": "https://www.odesk.com/", "helpCmsRoot": "https://www.odesk.com/helpcms/", "cookieDomain": ".odesk.com", "agoraCalTimeout": 5, "taskgenieURL": "http://front.agora.odesk.com:8090/", "orpcURL": "http://orpc.prod.sv4.odesk.com:83/orpc/", "navURL": "https://www.odesk.com"}}' \

*Staging*

	--custom-json '{"domain": "odesk.com","openldap": {"basedn": "dc=odesk,dc=com","server": "ldap.odesk.com"}, "statsd": {"enabled": true, "environment": "staging", "host": "statsd.odesk.com", "port": 8125 }, "newrelic": { "license": "b93eb98c61d9c195c85f188786271c29300780d1", "application_monitoring": { "enabled": true, "appname": "taskmanager-ui.staging"}, "php-agent": { "php_recipe": "php::configure", "config_file" : "/etc/php5/apache2/conf.d/newrelic.ini" } }, "is_dev_env": "false","static_url":"/taskmanager/static/", "static_path":"web/assets/","uglifyjs2_bin": "/usr/bin/uglifyjs","uglifycss_bin": "/usr/bin/uglifycss", "root_ext": "web","taskmanager": { "cookiePrefix": "", "webRoot": "https://s-www.odesk.com/", "helpCmsRoot": "https://s-www.odesk.com/helpcms/", "cookieDomain": ".odesk.com", "agoraCalTimeout": 15, "taskgenieURL": "http://front-staging.agora.odesk.com:8090/", "orpcURL": "http://orpc.staging1.sv4.odesk.com:83/orpc/", "navURL": "https://s-www.odesk.com"}}' \

*Development*

	--custom-json '{"domain": "odesk.com","openldap": {"basedn": "dc=odesk,dc=com","server": "ldap.odesk.com"}, "statsd": {"enabled": true, "environment": "dev", "host": "statsd.odesk.com", "port": 8125 }, "newrelic": { "license": "b93eb98c61d9c195c85f188786271c29300780d1", "application_monitoring": { "enabled": true, "appname": "taskmanager-ui.dev"}, "php-agent": { "php_recipe": "php::configure", "config_file" : "/etc/php5/apache2/conf.d/newrelic.ini" } }, "is_dev_env": "true","dev_env_name": "TASKMANAGER_ENVIRONMENT","static_url":"/taskmanager/static/", "static_path":"web/assets/","uglifyjs2_bin": "/usr/bin/uglifyjs","uglifycss_bin": "/usr/bin/uglifycss", "root_ext": "web","taskmanager": { "cookiePrefix": "dev02_", "webRoot": "http://dev02.odesk.com:23850/", "helpCmsRoot": "http://dev02.odesk.com:23850/helpcms/", "cookieDomain": ".dev02.odesk.com", "agoraCalTimeout": 50, "taskgenieURL": "http://front-dev.agora.odesk.com:8090/", "orpcURL": "http://dev02.odesk.com:23850/orpc/", "navURL": "http://dev02.odesk.com:23850"}}' \
	

Creating the Layers
-------------

NOTE: --region only influences Opsworks command & control - NOT instance region placement

	aws opsworks create-layer \
	  --region=us-east-1 \
	  --stack-id $STACK \
		--type php-app \
		--name "PHP App Server" \
		--shortname "php-app" \
		--custom-security-group-ids sg-534f5d3f \
		--enable-auto-healing \
		--no-install-updates-on-boot \
		--custom-recipes '{"Setup": [ "taskmanager::default", "apache2::mod_expires", "openldap::auth" ], "Deploy": ["taskmanager::sf2deploy"]}'

Creating the App:
-------------

	aws opsworks create-app \
		--region us-east-1 \
		--stack-id $STACK \
		--name "taskmanager-app" \
		--description "TaskManager UI Application" \
		--type php \
		--app-source '{"Url": "http://stash.odesk.com/scm/tg/taskmanager-ui.git", "Type": "git", "Revision": "master"}' \
		--no-enable-ssl 
	
Launching instances via CLI:
-------------

	# example using a JD dev stack
	aws opsworks create-instance \
		--region=us-east-1 \
		--stack-id $STACK \
		--layer-ids $LAYER \
		--instance-type c3.large

Run a deployment of the app
-------------

	aws opsworks create-deployment \
		--region=us-east-1 \
		--stack-id $STACK \
	    --app-id $APP \
		--command '{"Name":"deploy"}'

us-west-1a / staging => subnet-5ff23836
us-west-1a / prod => subnet-b6fc36df

For Vagrant setup
-------------

Run the following commands:

	$ git clone ssh://git@stash.odesk.com:7999/tg/taskgenie-ui.git # only if you don't have TaskManager UI code yet
	$ git clone ssh://git@stash.odesk.com:7999/siteops/chef-solo.git
	$ cd chef-solo/cookbooks/taskmanager
	$ vagrant box add taskmanagerui http://odesk-chef-solo-bucket.s3.amazonaws.com/virtualboxes/taskmanagerui.box
	$ vagrant up  # this will most likely take a while
	$ sudo chmod 777 data/logs # not needed on the Windows host
	$ sudo chmod 777 public/static # not needed on the Windows host
	$ browse to http://127.0.0.1:4568/taskmanager/demo in your browser

Notes. 

- The setup expects that your hosts' taskmanager UI code is at the same level as the chef-solo repo. For example checkout this setup:

	$ ptsakiri@~/projects$ ls

	chef-solo    taskgenie-ui

- On Windows hosts there can be a Compass error during the execution of ``vagrant up`` with a message like this:

        STDERR: Errno::ETXTBSY on line 886 of /opt/vagrant_ruby/lib/ruby/gems/1.8/gems/sass-3.2.12/lib/sass/../sass/util.rb: Text file busy

    Apply the following [solution](http://stackoverflow.com/questions/19300293/impossible-to-watch-with-compass-text-file-busy-on-shared-folder)
    to fix it:

    Add ``cache_path = "/tmp/.sass-cache"`` to the end of the ``taskgenie-ui/vendor/odesk/ui/css/v3/default/config.rb`` file.

    Then execute:

        vagrant halt
        vagrant up --provision

    to proceed with installation.

- The guest taskmanager UI VM talks to an orpc instance running on dev02 and the global dev instance of the TaskManager service, i.e http://front-dev.agora.odesk.com:8092/apidocs/#
