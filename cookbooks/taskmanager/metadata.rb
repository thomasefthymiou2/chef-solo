name             'taskmanager'
maintainer       'oDesk'
maintainer_email 'ptsakiri@odesk.com'
license          'All rights reserved'
description      'Installs/Configures taskmanager project'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apt-repo"
depends "newrelic"

recipe "taskmanager", "Installs apc and compass"
recipe "taskmanager::deploy", "Takes required steps for taskmanager ui deployment."
recipe "taskmanager::vinstall", "Install required packages for taskmager ui in vagrant."
recipe "taskmanager::vdeploy", "Takes required steps for taskmanager ui deployment in vagrant."