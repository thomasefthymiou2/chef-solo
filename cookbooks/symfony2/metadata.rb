name             'symfony2'
maintainer       'Thomas Efthymiou'
maintainer_email 'thomas.efthimiou@gmail.com'
license          'All rights reserved'
description      'Installs/Configures ui applet project'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apt-repo"

recipe "symfony2::vinstall", "Install required packages for symfony2 application (vagrant)."
recipe "symfony2::vdeploy", "Takes required steps for symfony2 application deployment (vagrant)."

