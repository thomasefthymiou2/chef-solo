# core software

apache_site 'default' do
  enable false
end
  
if platform_family?('debian')
  pkgs = %W[git git-core debconf-utils memcached php5 php-date php5-curl
    php5-memcached php5-memcache php5-intl ack-grep screen curl]
  
  # additional dependencies for non-packaged modules 
  pkgs.concat %W[libglib2.0-dev libgtop2-dev build-essential python-software-properties 
    python g++ make libapache2-svn php-apc]
    
  pkgs.each do |pkg|
    package pkg do
      action :install
    end
  end

  include_recipe 'apt-repo'
  ppa 'chris-lea/node.js'

  execute "apt-get update"
  package "nodejs" do
    action :install
  end
  
  execute "add 'apache' user" do
    command "useradd -o -u $(id -u www-data) apache -M -N"
    not_if "id apache"
  end

  execute "add 'apache' group" do
    command "groupadd -o -g $(id -g www-data) apache"
    not_if "grep apache /etc/group"
  end
  
end
