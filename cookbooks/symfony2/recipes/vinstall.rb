#
# Cookbook Name::ui-applet
# Recipe::vinstall
#
# Copyright 2014, oDesk
#
# All rights reserved - Do Not Redistribute
#

current_path = "/home/vagrant/#{node['project_path']}"

# Setup server name and document root for webserver
web_app "#{node['project_name']}_app" do
  server_name "#{node['project_name']}_app"
  docroot "#{current_path}/"  
  allow_override "All"
end

# Setup default php timezone that is required by applet
bash "Setup default PHP timezone" do
  user "root"
  code <<-EOH
  sed -i 's/;date.timezone.*/date.timezone = UTC/g' `php -r 'echo php_ini_loaded_file();'`
  EOH
end

# Install compass component
bash "install_compass" do
  cwd "/tmp"
  code <<-EOH  
  gem update --system
  gem install sass --version=3.2.12
  gem install compass
  EOH
end

# Install uglifycss and uglifyjs with npm
bash "install_uglify_components" do  
  cwd "/tmp"
  code <<-EOH  
  npm install uglify-js -g
  npm install uglifycss -g
  EOH
end

#install grunt-cli
bash "install_grunt" do
  cwd "/tmp"
  code <<-EOH
  npm install grunt-cli -g
  EOH
end
