#
# Cookbook Name::ui-applet
# Recipe::vdeploy
#
# Copyright 2013, oDesk
#
# All rights reserved - Do Not Redistribute
#

current_path = "/home/vagrant/#{node['project_path']}"

template "#{current_path}/app/config/parameters.yml" do
  local true
  source "#{current_path}/app/config/parameters.erb"
  mode 0644
end

bash "install_composer" do
  cwd "#{current_path}"
  code <<-EOH
  curl -s https://getcomposer.org/installer | php
  php composer.phar install
  EOH
end  
